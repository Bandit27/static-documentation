# Releasing your mod 
Now that you've built your mod, you just need to fill out the required information in the Edit Info page.

The Projects Description supports markdown, the Preview image will need to be square and both the preview image and web image need to below 2MB. Once you've finished that, you can use the release tab to read through the community guidelines and release the mod to get approved by staff.

# Does your mod need any dependencies?

If your mod needs any dependencies, we've got you covered. Add the required DLLs inside the folder called "Dependencies" and add it into the instructions.txt. Then when the user launches the game, the launcher will copy any dependencies into the Managed folder before they start the game. 