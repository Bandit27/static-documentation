# Finding what to name your textures

Sometimes the skin loader doesn't apply your skin because it can't find any matching materials.

Inside your game folder and then the Mod Loader folder. EG: `C:\Program Files (x86)\Steam\steamapps\common\VTOL VR\VTOLVR_ModLoader` a file called `gamedata.json` gets generated.

This contains some general information extracted from your last play session, including skin materials and what they should be renamed for a skin.

So if you load into the game, get to the point where the material in game is visible. Kill (Alt + F4) the game.

Then have a look at the bottom of `gamedata.json` it will have a JSON array of strings, it will say the texture it found that was loaded and what name to rename it to when loading your custom skin.

EG: `"\"tex_ah94_gun.png\" should be renamed to \"mat_ah94_gun.png\"",`