# Seeing your skin in-game
To view your skin in the game, you'll want to create a folder inside of your skins named the name what you want your skin to be called in-game.
Then you need to place a preview image with you can view before loading the skin. This is named 0.png, 1.png or 2.png which link to the vehicle. 

> 0.png = AV-42C
>
> 1.png = F/A-26B
>
> 2.png = F-45A
> 
> 3.png = AH-94
>
> preview.png = Some Custom vehicles


Then you'll want to name the texture to the material you'll want to change, in most cases, this is renaming the `tex_` at the front to `mat_` and then it should work. If you are unsure of what to name it, try looking at someone else skin for the same vehicle or have a look in the tips' page on how to use the `gamedata.json` file. 

# Custom Vehicles

At the time of writing this, some custom vehicles are now not replacing an existing plane but adding a new slot. This means the current skin system didn't work.

If the Mod Loader detects this plane, it will show every skin. Skin creators only need to include a `preview.png` file without a 0,1,2 or 3.png file. This will make it so the skin doesn't show up on any of the default vehicles in VTOL VR, but show up for custom vehicles.