GetSteamID returns the users steam id which can be used to identify the user's steam profile. This function uses Steamworks and returns as a ulong. 
```cs
Debug.Log("This is the users SteamID: " + VTOLAPI.instance.GetSteamID())
```