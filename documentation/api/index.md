This is the documentation for the mod loaders API

## Setup
>[!CAUTION]
> This is rarely used any more

To find the API all you need to do is get the instance variable from the static class then store that in a variable. Later on, all you have to call is "api" if you want to use a function from the API.

```cs
private VTOLAPI api;
private void Start()
{
    api = VTOLAPI.instance;
}
```