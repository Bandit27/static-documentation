MissionReloaded gets invoked whenever the user is in a mission and reloads it from a quicksave or restarts it from death.
```cs
public override void ModLoaded()
{
    VTOLAPI.MissionReloaded += MissionReloaded;
    base.ModLoaded();
}

private void MissionReloaded()
{
    //My Amazing Code to run when the user reloads the mission
}
```
You should also consider waiting for the flightscene to be ready, the following code in an IEnumerator will do that.
```cs
while (VTMapManager.fetch == null || !VTMapManager.fetch.scenarioReady || FlightSceneManager.instance.switchingScene)
    yield return null;
```
