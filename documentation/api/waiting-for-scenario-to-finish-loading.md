Run code when the scene has finished loading. 
```cs
public override void ModLoaded()
{
    VTOLAPI.SceneLoaded += SceneLoaded;
    base.ModLoaded();
}

private void SceneLoaded(VTOLScenes scene)
{
    switch (scene)
    {
        case VTOLScenes.ReadyRoom:
            break;
        case VTOLScenes.Akutan:
        case VTOLScenes.CustomMapBase:
            Log("Map Loaded");
            break;
        case VTOLScenes.LoadingScene:
            break;
    }
}
```
If you want to wait for the flightscene to be ready, consider adding the following code to an IEnumerator.
```cs
while (VTMapManager.fetch == null || !VTMapManager.fetch.scenarioReady || FlightSceneManager.instance.switchingScene)
    yield return null;
```
