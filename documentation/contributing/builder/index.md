# Builder

>[!WARNING]
> The Builder is old code and hasn't been refactored before. It does the job done on the one PC, anyone elses pc will cause errors (mainly with the paths of other programs).

The builder is tasked in building and packing the clients files for release. To do this it also uses other programs such as, msbuild and dotnet to complete its tasks.

Programs are listed in the `Dictionary`.
```cs
private static Dictionary<string, string> paths = new Dictionary<string, string>()
{
    { "msbuild", @"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe"},
    { "unity", @"C:\Program Files\Unity\Hub\Editor\2019.1.8f1\Editor\Unity.exe"},
    { "nuget", @"B:\Gitlab Runner\nuget.exe" },
    { "dotnet", @"C:\Program Files\dotnet\dotnet.exe"},
    { "sign", @"C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x64\signtool.exe"}
};
```
*[Program.cs Line 16 - 23](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/Builder/Program.cs#L16)*

These paths are hard coded to what is on the GitLab Runner's PC.

## Order of execution

The entry point of the project just sets up some variables and then checks the arguments passed in.

So the order of what method is called is defined in the [`.gitlab-ci.yml`](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/.gitlab-ci.yml) file.

At the start of the `.gitlab-ci` file, it builds the Builder with `dotnet build --configuration Release`, so any chances made to the builder will get updated.

Then in the next stages, this Builder.exe is moved to the root folder and called with arguments.

For example, this builds the Launcher
```yml
build_wpf:
  stage: build_wpf
  script:
   - .\Builder.exe buildwpf dlls=$dlls
  artifacts:
   paths:
   - Launcher\bin\Release\net5.0-windows\win-x64\publish\Launcher.exe
   expire_in: 1 day
  only:
    - dev
    - release
```
*[.gitlab-ci.yml Line 42 - 52](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/.gitlab-ci.yml#L42)*

## Argument `template=`

Example: `B:\Code\VTOL VR Modding\CICD Template`

The `template=` argument is a path to a folder which has the layout of the zip that gets uploaded to the site for release.

In this folder is some static files which rarely change on updates. This is what the folder looks like for the GitLab Runner on the repository:

- \VTOLVR_Data
- \VTOLVR_ModLoader
- \winhttp.dll
- \VTOLVR_Data\Managed
- \VTOLVR_Data\Plugins
- \VTOLVR_Data\Managed\0Harmony.dll
- \VTOLVR_Data\Managed\mscorlib.dll
- \VTOLVR_Data\Managed\SimpleTCP.dll
- \VTOLVR_Data\Plugins\discord-rpc.dll
- \VTOLVR_ModLoader\modloader.assets
- \VTOLVR_ModLoader\mods
- \VTOLVR_ModLoader\Mono.Cecil.dll
- \VTOLVR_ModLoader\skins

## Argument `dlls=`

Example: `B:\Code\VTOL VR Modding\ModLoader\dll`

`dlls=` is the path to where the external dependencies are located. All these files are in VTOL VR's Managed folder (`steamapps\common\VTOL VR\VTOLVR_Data\Managed`). The Builder copies all the files ended with `.dll` into the temporary folder GitLab Runners make when executing.


- \0Harmony.dll
- \Assembly-CSharp.dll
- \Assembly-CSharp-firstpass.dll
- \netstandard.dll
- \SimpleTCP.dll
- \Unity.TextMeshPro.dll
- \UnityEngine.AssetBundleModule.dll
- \UnityEngine.CoreModule.dll
- \UnityEngine.CrashReportingModule.dll
- \UnityEngine.dll
- \UnityEngine.PhysicsModule.dll
- \UnityEngine.UI.dll
- \UnityEngine.UnityWebRequestWWWModule.dll
- \Valve.Newtonsoft.Json.dll
- \Facepunch.Steamworks.Win64.dll

*[dll\instructions.txt](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/dll/instructions.txt)*
