# Improving Skins

The current method of skins of creating skins is not very user-friendly at all and hasn't been improved upon since the original idea was added into the Mod Loader.

The goal of this new feature would be to make it so the skin creators have to spend less time working out what to rename their png files inside file explorer. By proving them a menu inside the Launcher with autocorrect for common skin names.

# Making using JSON

Skins would make use of a new file called `skin.json` to store which texture should be applied to what material.

## Why a new file?

This is mainly for the back end, `info.json` is mostly generated from Django's REST API. So some copy and pasting happens on the server to keep extra info added into the info.json over when adding the information from Django.

So if we put this information into a different file, the server doesn't have to worry about losing this information. Just would check if the file exists, to make sure its a valid skin.

## What would be inside the `skin.json`

This json file will include:

- If this skin should only be applied to the local player (for MP support)
- Name of the material to apply it to
- The shader properties to apply it to
- What type this shader property is
- The value/file path to set.

### Example

```json
{
    "Name": "mat_ah94_ext",
    "ApplyToPlayer": true,
    "Textures": {
    "_Livery": {
        "$type": "Core.Classes.TextureTexture, Core",
        "DataType": 1,
        "FileName": null
    },
    "_EmissionColor": {
        "$type": "Core.Classes.TextureColour, Core",
        "DataType": 0,
        "R": 0.0,
        "G": 1.0,
        "B": 0.0,
        "A": 1.0
    },
    "_EmissionMap": {
        "$type": "Core.Classes.TextureTexture, Core",
        "DataType": 1,
        "FileName": "Emissive.png"
    }
}
```

# Possible user interface designs


# How it could be implemented

