# Roadmap

> [!WARNING]
> This roadmap is a vague plan of possible features to come to the Mod Loader. However, we are all doing this for fun and are unpaid for our time. So please don't force your self to stick to stuff and reach deadlines. Do what you feel like doing and what you'll enjoy.

# What these pages are

Each possible new feature will be split into individual pages, then on these pages people can write down more to expand upon the idea. This means that the main page won't be a massive pain to scroll down if the ideas grow.
