# Skins in multiplayer

> [!WARNING]
> This feature is reliant on the improving skins feature being complete as it requires the json to store extra information.

As multiplayer doesn't use the same config room, currently skins can not be used in multiplayer.

This will add the ability to select a skin to load and for other players modded users to see that skin.