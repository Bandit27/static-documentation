# AssetRipper

[![Github Image](https://img.shields.io/badge/Github-Repository-blue?style=for-the-badge&logo=github)](https://github.com/ds5678/AssetRipper) [![Latest Release of AssetRipper](https://img.shields.io/github/v/release/ds5678/AssetRipper?color=blue&label=Latest%20Release&logo=Github&style=for-the-badge)](https://github.com/ds5678/AssetRipper/releases/latest) ![Github Stars](https://img.shields.io/github/stars/ds5678/AssetRipper?logo=Github&style=for-the-badge)

> AssetRipper is a tool for extracting assets from serialized files (CAB-*, *.assets, *.sharedAssets, etc.) and assets bundles (*.unity3d, *.bundle, etc.) and converting them into the native Unity engine format.

We can use AssetRipper's simple GUI application to convert VTOL VR back into a useable Unity Project. This page will cover the sets on how to do that.

> [!WARNING]
> **AssetRipper does not export code**. 
> Currently, version 0.1.5.2 does not export code. However, the next release will support exporting code.

## Preparing your game folder
There are a few recommended steps to complete before you start decompiling your game so you get the true core files.

1. Move out your `VTOLVR_ModLoader` folder.

You can just drag out the folder for the time being into steam's `common` folder.

![Moved the VTOLVR_ModLoader folder out of the games files](/images/documentation/assetripper/moved-modloader.png)

2. If you have downloaded .mp3 files in your `RadioMusic`. Moves those also out of your game folder.

Most people tend to Stream music, but if you have any other songs downloaded and placed in here, it will save some time to move them out. Here is what the default folder looks like. 

![Default Radio Music Folder](/images/documentation/assetripper/default-radiomusic.png)

3. Verify your game files on steam.

The Mod Loader modifies core game files, so to get the correct export, verifying the files on steam will turn them back to their originals.

![Verify files button](/images/documentation/assetripper/steam-verfiyfiles.png)

## Downloading AssetRipper

[![Latest Release of AssetRipper](https://img.shields.io/github/v/release/ds5678/AssetRipper?color=blue&label=Latest%20Release&logo=Github&style=for-the-badge)](https://github.com/ds5678/AssetRipper/releases/latest)

As VTOL VR is only available for Windows, you will most likely need the `AssetRipperGUI_win64` zip. This can be found under the latest release.

![Download for windows 64](/images/documentation/assetripper/assetripper-download.png)

## Installing AssetRipper

Installing is simple, all you need to do is create a new folder called `AssetRipper` in your program files and extract the contents of the zip into that folder.

This is how it should look like.

![How it should look when installed](/images/documentation/assetripper/installed.png)

## Converting Game files

To open AssetRipper, just double click on `AssetRipper.exe`. Two windows should now open.

![Image of windows created by asset ripper](/images/documentation/assetripper/assetripper-windows.png)

The second window is what we will be dragging the VTOL VR folder into to begin the export.

![Dragging the VTOL VR Folder into AssetRipper](/images/documentation/assetripper/dragging-vtolfolder.gif) ![Full window](/images/documentation/assetripper/assetripper-fullwindow.png)

Now pressing Export > Export All Files will prompt you for the folder you want to extract the game files to.

![Export button](/images/documentation/assetripper/assetripper-export.png)

## Opening the files

Now that you've extracted all these files, you're going to want to open them.

> [!WARNING]
> It is highly recommended that you make a back of this exported folders incase you change something and want to remember how it was like in the default game files.

To find out what Unity version VTOL VR is current on, you can look at the top of your `player.log` file.
The log file is located in the games root folder. EG: `C:\Program Files (x86)\Steam\steamapps\common\VTOL VR\Player.log`

The third line down, you should see 
> Initialize engine version:
then the editor version.

This is the version you should download off Unity's [download archive](https://unity3d.com/get-unity/download/archive).

[![Download button for installer](/images/documentation/assetripper/unity-download.png)](https://unity3d.com/get-unity/download?thank-you=update&download_nid=61564&os=Win)

Once you have installed Unity. Open the `VTOLVR` folder as new project and after some time importing, it should open.

>[!NOTE]
> The first time you open it will take the longest. Once you've opened it once, future times will be much quicker.


