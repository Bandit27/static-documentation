The game has a debug camera which the developer uses to view around the different actors in the scene from his desktop. These are the controls for it.

> Insert - Enables/Disables
>
> [ - Moves back an actor in the list
>
> ] - Moves forward an actor in the list
>
> . - Speeds up time (0.1,0.25,0.5,1,2,4) (Single Player Only)
>
> , - Slows down time (0.1,0.25,0.5,1,2,4) (Single Player Only)
>
> / - Sets the time scale to 1
>
> Tab - Enables/Disables Gun shooting from mouse
>
> Left Shift + Scroll - Changes FOV
>
> h - Enables Head debug (Doesn't seem to work)
>
> v - Switches camera mode from free to chase and vice versa
>
> t - Does something called tgtMode (Doesn't seem to do anything)
> 
> RightShift + A - Changes something to do with Exterior Audio