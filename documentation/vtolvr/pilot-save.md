You can get a PilotSave from the PilotSaveManager class as long as you know the name of the save. Then you can set their pilot with PilotSaveManager.current.
```cs
PilotSaveManager.pilots; //Dictionary of the class 'PilotSave' with the key being their names'
PilotSaveManager.current; //To set the selected Pilot
```